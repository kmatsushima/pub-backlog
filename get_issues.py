import os
import requests
import json
import pandas as pd
import settings

BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/{api_url}'
API_URL = 'issues'
# parameters to change
PROJECT_ID = '2799'
OUTPUT_FILENAME = 'issues.csv'

def get_issues():
    url = BASE_URL.format(
        space_url=BACKLOG_SPACE_URL,
        api_url=API_URL,
    )
    # parameters to change
    params = {
        'apiKey': BACKLOG_API_KEY,
        'projectId[]': PROJECT_ID,
        # 'offset': 20,
    }
    r = requests.get(url, params=params)
    return r

if __name__ == '__main__':
    r = get_issues()
    response_obj = json.loads(r.text)
    df = pd.DataFrame(response_obj)
    output = df[['id', 'keyId', 'issueKey', 'summary']]
    output.to_csv(OUTPUT_FILENAME, index=False)
