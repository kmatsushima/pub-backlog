import os
import requests
import json
import pandas as pd
import settings
 
BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/projects/{project_id}/{api_url}'
API_URL = 'users'
# parameters to change
PROJECT_ID = '2799'
INPUT_FILENAME = 'project_users_to_add.xlsx'

def add_project_user(payload):
    url = BASE_URL.format(
        space_url=BACKLOG_SPACE_URL,
        project_id=PROJECT_ID,
        api_url=API_URL,
    )
    params = {
        'apiKey': BACKLOG_API_KEY,
    }
    r = requests.post(url, params=params)
    return r

if __name__ == '__main__':
    df = pd.read_excel(INPUT_FILENAME, header=1)

    for index, row in df.iterrows():
        payload={
            'userId': row['user_id'],
        }
        r = add_project_user(payload=payload)
        response_obj = json.loads(r.text)
        print(r, response_obj['summary'])
