import os
import requests
import json
import pandas as pd
import settings
 
BACKLOG_SPACE_URL = os.environ.get('BACKLOG_SPACE_URL')
BACKLOG_API_KEY = os.environ.get('BACKLOG_API_KEY')
BASE_URL = 'https://{space_url}.backlog.com/api/v2/projects/{project_id}/{api_url}'
API_URL = 'users'
# parameters to change
PROJECT_ID = '2799'
OUTPUT_FILENAME = 'project_users.csv'

if __name__ == '__main__':
    url = BASE_URL.format(
        space_url=BACKLOG_SPACE_URL,
        project_id=PROJECT_ID,
        api_url=API_URL,
    )
    params = {
        'apiKey': BACKLOG_API_KEY,
    }
    r = requests.get(url, params=params)
    user_obj = json.loads(r.text)
    df = pd.DataFrame(user_obj)
    df.to_csv(OUTPUT_FILENAME)
